### DOCKER FILE FOR ldap IMAGE ###

###
# export RELEASE_VERSION=":v0"
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/ldap${RELEASE_VERSION} -f ldap.Dockerfile .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/ldap${RELEASE_VERSION}
###


FROM osixia/openldap:1.2.0

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# Specify TLS certificate and key
#ADD ./secrets/ldap.* /container/service/slapd/assets/certs/
#ENV LDAP_TLS_CRT_FILENAME=ldap.crt
#ENV LDAP_TLS_KEY_FILENAME=ldap.key

# Add customized schema for Web SSO to Unix Account mapping
RUN mkdir /container/service/slapd/assets/config/bootstrap/schema/custom
ADD ./ldap.d/sso.schema /container/service/slapd/assets/config/bootstrap/schema/custom/sso.schema

# Copy a script to add dummy users and admin to LDAP for testing purposes
ADD ./ldap.d/addusers.sh /root/

# Start the LDAP service and bootstrap it the first time
ENTRYPOINT /container/tool/run --copy-service
