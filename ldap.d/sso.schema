###
# Web SSO to Unix custom schema
#
# Nomenclature: Web to Unix --> Web2Unix --> w2u
###

### Object Identifiers
objectidentifier w2uSchema 1.3.6.1.4.1.37.23
objectidentifier w2uAttrs w2uSchema:1
objectidentifier w2uOCs w2uSchema:2

### Attributes
#attributetype ( w2uAttrs:1 
#				NAME 'name'
#				DESC 'Base name attribute'
#				EQUALITY caseIgnoreMatch
#				SUBSTR caseIgnoreSubstringsMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} )
#
attributetype ( w2uAttrs:2
				NAME 'ssouid'
				DESC 'SSO-provided User Identifier' 
				SUP name )
# Already defined in /etc/ldap/schema/inetorgperson.schema
#attributetype ( w2uAttrs:3
#				NAME 'displayname'
#				DESC 'SSO-provided Display Name'
#				SUP name )
#
# Already defined in /etc/ldap/schema/core.schema
#attributetype ( w2uAttrs:4
#				NAME ( 'givenName' 'gn' )
#				DESC 'SSO-provided Given Name -- RFC2256'
#				SUP name )
#
# Already defined in /etc/ldap/schema/core.schema
#attributetype ( w2uAttrs:5
#				NAME ( 'sn' 'surname' )
#				DESC 'SSO-provided Surname -- RFC2256'
#				SUP name )
#
# Already defined in /etc/ldap/schema/core.schema
#attributetype ( w2uAttrs:6
#				NAME ( 'mail' 'rfc822Mailbox' )
#				DESC 'SSO-provided email address -- RFC1274: RFC822 Mailbox'
#				EQUALITY caseIgnoreIA5Match
#				SUBSTR caseIgnoreIA5SubstringsMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} )
###
#attributetype ( w2uAttrs:7
#				NAME ( 'uid' 'userid' )
#				DESC 'User Identifier -- RFC1274'
#				EQUALITY caseIgnoreMatch
#				SUBSTR caseIgnoreSubstringsMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{256} )
#attributetype ( w2uAttrs:8
#				NAME 'uidNumber'
#				DESC 'An integer uniquely identifying a user in an administrative domain'
#				EQUALITY integerMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 SINGLE-VALUE )
#attributetype ( w2uAttrs:9
#				NAME 'gidNumber'
#				DESC 'An integer uniquely identifying a group in an administrative domain'
#				EQUALITY integerMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 SINGLE-VALUE )
#attributetype ( w2uAttrs:10
#				NAME ( 'cn' 'commonName' )
#				DESC 'Common name for which the entity is known by -- RFC2256'
#				SUP name )
#
# Already defined in /etc/ldap/schema/nis.schema
#attributetype ( w2uAttrs:11
#				NAME 'homeDirectory'
#				DESC 'The absolute path to the home directory'
#				EQUALITY caseExactIA5Match
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
#
# Already defined in /etc/ldap/schema/nis.schema
#attributetype ( w2uAttrs:12
#				NAME 'loginShell'
#				DESC 'The path to the login shell'
#				EQUALITY caseExactIA5Match
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
#
# Already defined in /etc/ldap/schema/nis.schema
#attributetype ( w2uAttrs:13
#				NAME 'gecos'
#				DESC 'The GECOS field'
#				EQUALITY caseIgnoreIA5Match
#				SUBSTR caseIgnoreIA5SubstringsMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
#attributetype ( w2uAttrs:14
#				NAME 'userPassword'
#				DESC 'Password of User -- RFC2256/2307'
#				EQUALITY octetStringMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.40{128} )
#
# Already defined in /etc/ldap/schema/core.schema
#attributetype ( w2uAttrs:15
#				NAME 'description'
#				DESC 'Descriptive Information -- RFC2256'
#				EQUALITY caseIgnoreMatch
#				SUBSTR caseIgnoreSubstringsMatch
#				SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{1024} )
#
# Already defined in /etc/ldap/schema/core.schema
#attributetype ( w2uAttrs:16
#				NAME ( 'o' 'organizationName' )
#				DESC 'Irganization this object belongs to -- RFC2256'
#				SUP name )
#
# Already defined in /etc/ldap/schema/core.schema
#attributetype ( w2uAttrs:17
#				NAME ( 'ou' 'organizationalUnitName' )
#				DESC 'Organizational unit this object belongs to -- RFC2256'
#				SUP name )
###

### Object Classes
# SSO-provided Attributes
objectClass ( w2uOCs:1
				NAME 'ssoAttributes'
				DESC 'Describe attributes provided by the Web SSO'
				SUP ( top ) AUXILIARY
				MUST (
					ssouid
					) 
				MAY (
					name $ displayname $ givenname $ sn $ mail
					)
			)
# Unix Account Attributes
objectClass ( w2uOCs:2
				NAME 'unixAccount'
				DESC 'Describe attributes associated to a Unix account'
				SUP ( top ) AUXILIARY
				MUST (
					uid $ uidNumber $ gidNumber
					) 
				MAY (
					cn $ description $ o $ ou $
					homeDirectory $ loginShell $ gecos $ userPassword
					)
			)
# SSO -- Unix Match Object Class
objectClass ( w2uOCs:3
				NAME 'ssoUnixMatch'
				DESC 'Describe a Mapping between SSO-provided attributes and Unix account'
				SUP ( top ) AUXILIARY
				MUST (
					ssouid $
					uid $ uidNumber $ gidNumber
					) 
				MAY (
					name $ displayname $ givenname $ sn $ mail $
					cn $ description $ o $ ou $
					homeDirectory $ loginShell $ gecos $ userPassword
					)
			)
###
