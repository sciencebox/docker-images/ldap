## LDAP Server for Boxed
Since version 0.1 of Docker image 'gitlab-registry.cern.ch/cernbox/boxedhub/ldap:v0.1', the LDAP server embeds a customized schema (please refer to the 'sso.schema' file) to support the mapping between user identities provided by a Single Sign-On (SSO) solution and Unix accounts information dinamically generated upon the arrival of a new user.
This mapping allows for retrieval of usable (i.e., Unix account compliant) information from all containers part of Boxed.

Additional intelligence is needed on the CERNBox and on the SWAN side to successfully register the mapping between Web and Unix identities and to login a user from the SSO.
Specifically:
  * CERNBox will be responsible for registering the new web identity in the LDAP server. Unix account information will be automatically generated. *Note: Such feature requires the use of 'UserBackendSSOtoLDAP.php' as user backend*;
  * CERNBox and SWAN will be able to fetch Unix user information given a web identity from the LDAP server. *Note: SWAN will the require the use of 'up2u\_authenticator' to access the access the LDAP server and query for the required information*.


## Examples of supported attributes by the customized schema

### Unix account entry -- ldif file
```
dn: uid=user1000,dc=example,dc=org
objectclass: top
objectclass: account
objectclass: unixAccount
uid: user1000
uidNumber: 1000
gidNumber: 1000
cn: user1000
homeDirectory: /root/homes/user1000
loginShell: /bin/bash
```

### SSO account entry -- ldif file
```
dn: ssouid=sso1000,dc=example,dc=org
objectclass: top
objectclass: account
objectclass: ssoAttributes
uid: sso1000
ssouid: sso1000
displayname: "My Name"
givenname: "My"
sn: "Name"
mail: "my.name@mail.com"
```

### SSO - Unix matching entry -- ldif file
```
dn: uid=user1000,ssouid=sso1000,dc=example,dc=org
objectClass: top
objectClass: account
objectClass: ssoUnixMatch
ssouid: sso1000
displayName: "My Name"
givenName: "My"
sn: "Name"
mail: "my.name@mail.com"
uid: user1000
uidNumber: 1000
gidNumber: 1000
cn: user1000


```
